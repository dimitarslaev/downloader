const { Server } = require("socket.io");
const http = require("http");
const { Downloader } = require("./downloader.js");

const httpServer = http.createServer();

const io = new Server(httpServer, {
  cors: {
    origin: "*",
  },
});

io.on("connection", (socket) => {
  socket.on("form-submitted", (data) => {
    try {
      const downloader = new Downloader({
        ...data,
        onComplete: ({ downloads, zipFile }) => {
          socket.emit("complete", downloads);
          socket.emit("zipFile", zipFile);
        },
        onProgress: (percentage) => {
          socket.emit("progress", percentage);
        },
        onError: (error) => {
          console.log(error);
          socket.emit("error", error.message);
        },
      });

      downloader.init();
    } catch (error) {
      console.log(error);
      socket.emit("error", error.message);
    }
  });
});

httpServer.listen(8080);
