import ReactDOM from "react-dom";
import React, { useState, useEffect, useRef } from "react";
import io from "socket.io-client";
import {
  Grid,
  TextField,
  Paper,
  Button,
  Box,
  Alert,
  AlertTitle,
  LinearProgress,
  Typography,
  Snackbar,
  RadioGroup,
  Radio,
  FormControlLabel,
} from "@mui/material";
import TreeView from "@mui/lab/TreeView";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import TreeItem from "@mui/lab/TreeItem";

export const App = () => {
  const [error, setError] = useState("");
  const [downloads, setDownloads] = useState([]);
  const [zipFile, setZipFile] = useState(null);
  const [progress, setProgress] = useState(0);
  const [uploadFile, setUploadFile] = useState(null);
  const socket = useSocket({
    port: 8080,
    setProgress,
    setDownloads,
    setZipFile,
    setError,
  });

  const onSubmit = (e) => {
    e.preventDefault();

    setDownloads([]);
    setError("");

    if (!socket?.connected) {
      setError("Няма връзка със сървъра");
      return;
    }

    const formData = new FormData(e.target);

    if (formData.get("input-file").name === "") {
      setError("Няма прикачен файл");
      return;
    }

    const data = {
      file: formData.get("input-file"),
      csvUrlColumn: formData.get("input-column-url"),
      csvSkuColumn: formData.get("input-column-sku"),
      imageResizeWidth: parseInt(formData.get("input-image-size"), 10),
      skuFolders: formData.get("input-sku-folders") === "0" ? false : true,
    };

    socket.emit("form-submitted", data);
  };

  const onChangeFile = (e) => {
    const file = e.target.files[0];

    if (!file || !file.name.toLowerCase().includes("csv")) {
      setError("Невалиден файл.");
      return;
    }

    setUploadFile(file);
  };

  return (
    <>
      <Layout
        leftContent={
          <form onSubmit={onSubmit}>
            <TextField
              name="input-image-size"
              label="На колко пиксела да се конвертира снимката?"
              type="number"
              defaultValue={1000}
              helperText="300px до 2000px"
              variant="filled"
              fullWidth
              sx={{ mb: 2 }}
            />

            <TextField
              name="input-column-sku"
              label="Заглавие на колоната за каталожен номер"
              defaultValue="SKU"
              helperText="по подразбиране SKU"
              variant="filled"
              fullWidth
              sx={{ mb: 2 }}
            />

            <TextField
              name="input-column-url"
              label="Заглавие на колоната за хиперлинк"
              defaultValue="URL"
              helperText="по подразбиране URL"
              variant="filled"
              fullWidth
              sx={{ mb: 2 }}
            />

            <RadioGroup
              defaultValue="0"
              name="input-sku-folders"
              sx={{ mb: 2 }}
            >
              <FormControlLabel
                value="0"
                control={<Radio />}
                label="Всички файлове в една папка"
              />
              <FormControlLabel
                value="1"
                control={<Radio />}
                label="Папка за всяко SKU"
              />
            </RadioGroup>

            <Box display="flex" alignItems="center" sx={{ mb: 2 }}>
              <Button variant="contained" color="inherit" component="label">
                Прикачи файл
                <input
                  type="file"
                  name="input-file"
                  hidden
                  onChange={onChangeFile}
                />
              </Button>

              {uploadFile && (
                <Typography sx={{ ml: 2 }}>{uploadFile.name}</Typography>
              )}
            </Box>

            <LinearProgressWithLabel value={progress} />

            <Button
              type="submit"
              variant="contained"
              disabled={!uploadFile || !!zipFile}
              sx={{ mt: 2 }}
            >
              Старт!
            </Button>

            {zipFile && (
              <Button
                variant="contained"
                color="primary"
                href={zipFile}
                sx={{ mt: 2, ml: 2 }}
              >
                Свалени файлове (ZIP)
              </Button>
            )}
          </form>
        }
        rightContent={
          <Box
            sx={{
              bgcolor: "grey.300",
              p: 4,
              maxHeight: 400,
              height: "100%",
              overflowY: "auto",
            }}
          >
            <MemoizedTree downloads={downloads} />
          </Box>
        }
      />

      <Snackbar
        open={Boolean(error)}
        autoHideDuration={3000}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        onClose={() => setError("")}
        sx={{
          minWidth: 300,
        }}
        message={error}
      ></Snackbar>
    </>
  );
};

export const Layout = ({ leftContent, rightContent }) => {
  return (
    <Box
      container
      sx={{
        height: "100vh",
        width: "100vw",
        bgcolor: "grey.300",
        boxSizing: "border-box",
        py: 6,
        px: 4,
      }}
    >
      <Paper
        sx={{
          width: "100%",
          maxWidth: 1200,
          minWidth: 960,
          maxHeight: "80%",
          margin: "0 auto",
          overflow: "hidden",
        }}
      >
        <Grid container spacing={4} p={4}>
          <Grid item xs={6}>
            <Grid container spacing={4}>
              <Grid item xs={12}>
                {leftContent}
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <Grid container spacing={4} sx={{ height: "100%" }}>
              <Grid item xs={12} sx={{ height: "100%" }}>
                {rightContent}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </Box>
  );
};

export const LinearProgressWithLabel = (props) => {
  return (
    <Box sx={{ display: "flex", alignItems: "center" }}>
      <Box sx={{ width: "100%", mr: 1 }}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="text.secondary">{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  );
};

export const Error = ({ error }) => {
  if (!error) return null;

  return (
    <Alert severity="error" variant="filled">
      <AlertTitle>Грешкa</AlertTitle>
      {error}
    </Alert>
  );
};

export const ERRORS = {
  "still-running": "Сървъра още работи по последната задача",
  "missing-file": "Няма прикачен файл",
  "error-parse-file": "Грешка при прочитането на файла",
  "error-sku-column": "Колоната за каталожен номер не съответсва",
  "error-url-column": "Колоната за лихиперлинкнк не съответсва",
  "error-moving-image": "Грешка при конвертирането на снимката",
  "error-moving-doc": "Грешка при свалянето на документ",
  "error-download-file": "Грешка при свалянето на файла",
};

export const Tree = ({ downloads }) => {
  const fileNameSort = (a, b) => a.name.localeCompare(b.name);
  console.log(downloads);
  return (
    <TreeView
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
    >
      {downloads.length > 0 &&
        downloads
          .sort(fileNameSort)
          .map(({ url, name, path, error }) => (
            <TreeItem
              key={path}
              nodeId={path}
              label={error ? url + " - " + error : name}
              sx={error ? { color: "error.main" } : null}
            ></TreeItem>
          ))}
    </TreeView>
  );
};

const MemoizedTree = React.memo(Tree);

export const useSocket = ({
  port,
  setProgress,
  setDownloads,
  setZipFile,
  setError,
}) => {
  const socketRef = useRef(io("ws://localhost:" + port));

  useEffect(() => {
    console.log("socketEffect", socketRef);

    socketRef.current.on("disconnect", () => {
      setError("Няма връзка със сървъра");
    });

    socketRef.current.on("progress", (percentage) => {
      setProgress(percentage);
    });

    socketRef.current.on("complete", (downloads) => {
      setDownloads(downloads);
    });

    socketRef.current.on("zipFile", (zipFile) => {
      setZipFile(zipFile);
    });

    socketRef.current.on("error", (error) => {
      console.log(error);
      setError(ERRORS[error] || "Непозната грешка");
    });

    return () => {
      socketRef.current.off("disconnect");
      socketRef.current.off("progress");
      socketRef.current.off("zipFile");
      socketRef.current.off("error");
    };
  }, []);

  return socketRef.current;
};

ReactDOM.render(<App />, document.getElementById("app"));
export default App;
