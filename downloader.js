const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");
const FileDownloader = require("nodejs-file-downloader");
const sharp = require("sharp");
const AdmZip = require("adm-zip");

const Utils = {
  imageExt: ["jpeg", "jpg", "png", "webp", "gif", "avif", "tiff", "svg"],

  docExt: ["pdf", "doc", "docx"],

  getExtension: function (fileName) {
    return path.extname(fileName).replace(".", "").toLowerCase();
  },

  getFileName: function (url) {
    return url.split("/").pop();
  },

  getSkuFileName: function (sku, index, ext) {
    return index > 0 ? `${sku}_${index}.${ext}` : `${sku}.${ext}`;
  },

  isImage: function (fileName) {
    const ext = this.getExtension(fileName);
    return this.imageExt.includes(ext);
  },
};

const ERROR_MISSING_FILE = "missing-file";
const ERROR_PARSE_FILE = "error-parse-file";
const ERROR_SKU_COLUMN = "error-sku-column";
const ERROR_URL_COLUMN = "error-url-column";
const ERROR_MOVING_IMAGE = "error-moving-image";
const ERROR_MOVING_DOC = "error-moving-doc";
const ERROR_DOWNLOAD_FILE = "error-download-file";

class Downloader {
  constructor(options) {
    this.options = {
      file: null,
      csv: { delimiter: ",", columns: true },
      csvUrlColumn: "URL",
      csvSkuColumn: "SKU",
      outputDir: "./public/files",
      archiveDir: "./public/archives",
      skuFolders: true,
      imageResizeWidth: 1000,
      onComplete: () => {},
      onProgress: (percentage) => {},
      onError: () => {},
      ...options,
    };

    this.state = {
      tree: {},
      total: 0,
      progress: 0,
      downloads: [],
      zipFile: null,
    };
  }

  init() {
    if (!this.options.file || !this.options.file.toString().length) {
      throw new Error(ERROR_MISSING_FILE);
    }

    this.makeDirs();
    this.parseCSV()
      .then(this.downloadFiles.bind(this))
      .then(this.moveFiles.bind(this))
      .then(this.zipDownloads.bind(this))
      .then(this.finish.bind(this))
      .catch(this.options.onError);
  }

  makeDirs() {
    console.debug("makeDirs");
    try {
      fs.rmdirSync(this.options.outputDir, { recursive: true });
      fs.mkdirSync(this.options.outputDir);
    } catch {}
  }

  parseCSV() {
    console.debug("parseCSV");
    return new Promise((resolve, reject) => {
      parse(this.options.file, this.options.csv, (err, records) => {
        if (err) reject(ERROR_PARSE_FILE);
        this.state.total = records.length;
        resolve(records);
      });
    });
  }

  downloadFiles(parsedData) {
    console.debug("downloadFiles");
    return Promise.all(
      parsedData.map(async (row) => {
        const sku = row[this.options.csvSkuColumn];
        const url = row[this.options.csvUrlColumn];

        if (!sku) {
          throw new Error(ERROR_SKU_COLUMN);
        }

        if (!url) {
          throw new Error(ERROR_URL_COLUMN);
        }

        const file = { sku, url };

        try {
          const downloader = new FileDownloader({
            url,
            directory: `${this.options.outputDir}/temp/${sku}`,
            cloneFiles: false,
            maxAttempts: 1,
            headers: {
              "User-Agent": "Mozilla/5.0",
            },
          });
          const { filePath } = await downloader.download();
          file.path = filePath;
          file.type = Utils.isImage(filePath) ? "image" : "document";
        } catch (error) {
          file.error = ERROR_DOWNLOAD_FILE;
        }

        this.state.downloads.push(file);
        this.updateProgress();
      })
    );
  }

  moveFiles() {
    const tree = {};

    this.state.downloads.forEach(({ sku, type }, index) => {
      tree[sku] = tree[sku] || {};
      tree[sku].images = tree[sku].images || [];
      tree[sku].docs = tree[sku].docs || [];

      if (type === "image") {
        tree[sku].images.push(index);
      } else {
        tree[sku].docs.push(index);
      }
    });

    return Promise.all(
      Object.keys(tree).map(async (sku) => {
        await Promise.all(
          tree[sku].images.map(async (fileIndex, index) => {
            await this.moveFile(fileIndex, index);
          })
        );

        await Promise.all(
          tree[sku].docs.map(async (fileIndex, index) => {
            await this.moveFile(fileIndex, index);
          })
        );
      })
    );
  }

  async moveFile(fileIndex, index) {
    const file = this.state.downloads[fileIndex];

    try {
      const ext = Utils.getExtension(file.path);
      const skuFileName = Utils.getSkuFileName(file.sku, index, ext);

      let newPath = "";
      if (this.options.skuFolders) {
        const dir = `${this.options.outputDir}/${file.sku}`;
        newPath = `${dir}/${skuFileName}`;
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
      } else {
        newPath = `${this.options.outputDir}/${skuFileName}`;
      }

      if (file.type === "image") {
        await this.resizeImage(file.path, newPath);
      } else {
        fs.renameSync(file.path, newPath);
      }

      file.name = skuFileName;
      file.path = newPath;
    } catch (error) {
      if (file.type === "image") {
        file.error = ERROR_MOVING_IMAGE;
      } else {
        file.error = ERROR_MOVING_DOC;
      }
      console.debug(error.message, error.stack, file);
    }
  }

  async resizeImage(path, newPath) {
    const resizeWidth = this.options.imageResizeWidth;
    const image = sharp(path);
    const metadata = await image.metadata();

    if (metadata.width <= resizeWidth) {
      fs.renameSync(path, newPath);
    } else {
      await image.resize({ width: resizeWidth }).toFile(newPath);
      fs.unlinkSync(path);
    }
  }

  updateProgress(progress) {
    if (progress) {
      this.options.onProgress(progress);
      return;
    }
    this.state.progress = Math.ceil(
      (this.state.downloads.length / this.state.total) * 100
    );
    this.options.onProgress(this.state.progress);
  }

  async zipDownloads() {
    fs.rmdirSync(`${this.options.outputDir}/temp`, { recursive: true });
    const zip = new AdmZip();
    const fileName = `downloads-${new Date().toLocaleDateString("bg-BG", {
      year: "numeric",
      month: "numeric",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    })}.zip`
      .replace(" г., ", "-")
      .replace(" ч.", "");
    const outputFile = `${this.options.archiveDir}/${fileName}`;

    await zip.addLocalFolderPromise(this.options.outputDir);
    await zip.writeZipPromise(outputFile);

    this.state.zipFile = outputFile.startsWith("./")
      ? outputFile.replace("./", "")
      : outputFile;
    console.log(`Created ${outputFile} successfully`);
  }

  finish() {
    fs.rmdirSync(this.options.outputDir, { recursive: true });
    this.options.onComplete({
      downloads: this.state.downloads,
      zipFile: this.state.zipFile,
    });
    this.updateProgress(100);
    this.state.downloads = [];
    console.debug("finish");
  }
}

module.exports = {
  Utils,
  Downloader,
};
