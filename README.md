### Downloader

- Requirements - https://drive.google.com/file/d/1PBawFlKfBsf82qUVy7T5dZWdpPfASG5T/view?ts=62eba516

### How to use

- [Install Node.js](https://nodejs.org/en/download/) (if not installed)
- In terminal, run `npm install` to install required packages
- In terminal, run `npm start` to start server and client application
- In browser, open http://localhost:1234
- Use `example-data.csv` to test
